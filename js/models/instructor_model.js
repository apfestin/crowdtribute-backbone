var CrowdTribute = CrowdTribute || {};
CrowdTribute.Models = CrowdTribute.Models || {};

CrowdTribute.Models.InstructorModel = Backbone.Model.extend({
  initialize: function(options) {
    if(options) {
      this.courseID = parseInt(options.course_id);
      this.instructorID = parseInt(options.instructor_id)
    }
  },
  urlRoot: function() {
    return 'http://localhost:3000/courses/' + this.courseID + '/instructors'
  }
});