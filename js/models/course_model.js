var CrowdTribute = CrowdTribute || {};
CrowdTribute.Models = CrowdTribute.Models || {};

CrowdTribute.Models.Course = Backbone.Model.extend({
  urlRoot: 'http://localhost:3000/courses',
  validate: function(attrs) {
    if(attrs.name.length <= 0) {
      return 'Please fill up name';
    }
    if(attrs.title.length <= 0) {
      return 'Please fill up title';
    }
  }});


