var CrowdTribute = CrowdTribute || {};
CrowdTribute.Models = CrowdTribute.Models || {};

CrowdTribute.Models.CourseMaterialModel = Backbone.Model.extend({
  initialize: function(options) {
    if(options) {
      this.courseID = parseInt(options.course_id);
      this.instructorID = parseInt(options.instructor_id);
      this.courseMaterialID = parseInt(options.course_material_id);
    }
  },
  urlRoot: function() {
    return 'http://localhost:3000/courses/' + this.courseID + '/instructors/' + this.instructorID + '/course_materials'
  }
});