// CrowdTribute.Models.Instructor = Backbone.Model.extend({});
// CrowdTribute.Models.CourseMaterials = Backbone.Model.extend({});


// var Book = Backbone.Model.extend({urlRoot : '/books'});
// var solaris = new Book({id: "1083-lem-solaris"});
// solaris.fetch();


// Collections
//   1. Courses
//   2. Instructors (of a course)
//   3. Course Materials (of an instructor)

// Models
//   1. Course
//   2. Instructor
//   3. Course Materials

// Views
//   1. CourseView
//   2. CourseListView

var CrowdTribute =  CrowdTribute || {}
CrowdTribute.Collections = CrowdTribute.Collections || {}
CrowdTribute.Models = CrowdTribute.Models || {}
CrowdTribute.Templates = CrowdTribute.Templates || {}
CrowdTribute.Views = CrowdTribute.Views || {}
CrowdTribute.Routers = CrowdTribute.Routers || {}



