var app = app || {}
var CrowdTribute = CrowdTribute || {}
CrowdTribute.View = CrowdTribute.Views || {}

var AboutController = {
  showAbout: function() {
    var aboutView = new CrowdTribute.Views.AboutView();
    $(".jumbo-container").hide();
    console.log('fff');
    $("#body").html(aboutView.render().el);
  }
}

var CoursesController = {
  showCourse: function(course_id) {
    var coursePageView = new CrowdTribute.Views.CoursePageView({course_id: course_id});
    $('#body').html(coursePageView.render().el);
  }
}

var InstructorsController = {
  showInstructor: function(course_id, instructor_id) {
    var instructorPageView = new CrowdTribute.Views.InstructorPageView({course_id: course_id, instructor_id: instructor_id});
    $("#body").html(instructorPageView.render().el);
  }
}

var HomeController = {
  showHome: function() {
    list = new CrowdTribute.Collections.CourseList();
    $('#body').html('');
    $('.jumbo-container').show();
    coll = new CrowdTribute.Views.CourseListView({ collection: list });  
    
  }
}

// ************** ROUTES *******************

var Application = Backbone.Router.extend({
  routes: {
    'about' : 'goToAbout',
    'courses/:course_id' : 'goToCourse',
    'courses/:course_id/instructors/:instructor_id' : 'goToInstructor',
    '' : 'goToHome'
  },

  goToAbout: function() {
    AboutController.showAbout();
  },

  goToCourse: function(course_id) {
    CoursesController.showCourse(course_id);
  },

  goToInstructor: function(course_id, instructor_id) {
    InstructorsController.showInstructor(course_id, instructor_id);
  },

  goToHome: function() {
    HomeController.showHome()
  }
});

// ************** INIT *******************

CrowdTribute.App = new Application();
Backbone.history.start();

