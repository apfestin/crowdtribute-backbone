var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.CourseMaterialListView = Backbone.View.extend({
  tagName: 'table',
  className: 'table table-striped course-material-list',
  initialize: function(options) {
    this.listenTo(this.collection, 'reset', this.render);

    console.log(options);

    this.courseID = parseInt(options.course_id);
    this.instructorID = parseInt(options.instructor_id);



    this.collection.fetch({reset: true});
  },
  render: function() {
    var self = this;

    self.$el.html('');

    self.$el.append('<tr><th>Title</th><th>Description</th><th></th>');

    _.each(this.collection.models, function(courseMaterial) {
      courseMaterialView = new CrowdTribute.Views.CourseMaterialView({model: courseMaterial, course_id: self.collection.courseID, instructor_id: self.collection.instructorID});
      self.$el.append(courseMaterialView.render().el);
    });

    return this;
  }
});