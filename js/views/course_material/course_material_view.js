var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.CourseMaterialView = Backbone.View.extend({
  tagName: 'tr',
  template: _.template($('#course-material-template').html()),
  initialize: function(options) {

    this.listenTo(this.model, 'change', this.render);
    this.listenTo(this.model, 'destroy', this.remove);


    this.courseID = parseInt(options.course_id);
    this.instructorID = parseInt(options.instructor_id);

    console.log(this.courseID);
  },
  events: {
    'click .edit-btn' : 'editCourseMaterial',
    'click .delete-btn' : 'deleteCourseMaterial',
    'click .update-btn' : 'updateCourseMaterial',
    'click .cancel-btn' : 'cancelEdit'
  },

  render: function() {
    compiledHTML = this.template(_.extend(this.model.toJSON(), {courseID: this.courseID, instructorID: this.instructorID}));
    this.$el.html(compiledHTML);

    return this;
  },

  cancelEdit: function() {
    this.$('.editable').hide();
    this.$('.default-shown').show();
  },

  editCourseMaterial: function() {
    this.$('.editable').show();
    this.$('.default-shown').hide();
  },

  deleteCourseMaterial: function() {
    console.log('delete');
    this.model.destroy();
  },

  updateCourseMaterial: function() {
    console.log('delete');
    this.model.save({title: this.$('.title-editable').val(), 
                     description: this.$('.description-editable').val()});
  }
});