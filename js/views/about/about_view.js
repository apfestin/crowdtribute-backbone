CrowdTribute.Views.AboutView = Backbone.View.extend({
  tagName: 'div',
  template: _.template('CrowdTribute brings together both students and teachers in need of course materials.'),
  render: function() {
    this.$el.html(this.template);

    return this;
  }

});