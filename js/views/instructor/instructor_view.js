var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.InstructorView = Backbone.View.extend({
  tagName: 'tr',
  template: _.template("<td><%= last_name %></td><td><%= first_name %></td><td><a href='#courses/<%= courseID %>/instructors/<%= id %>'>Show Course Materials</a></td>"),
  initialize: function(options) {
    this.courseID = parseInt(options.course_id);
  },
  render: function() {
    compiledHTML = this.template(_.extend(this.model.toJSON(), {courseID: this.courseID}));
    this.$el.html(compiledHTML);

    return this;
  }
});