var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.InstructorDescriptionView = Backbone.View.extend({
  tagName: 'div',
  template: _.template("<h3>Instructor: <%= safeName() %></h3>"),
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);

    this.model.fetch({reset: true});
  },
  render: function() {
    var self = this;
    var compiledHTML = this.template(_.extend(this.model.toJSON(), {
      safeName: function() {
        if(self.model.get('id')) {
          return self.model.get('last_name') + ", " + self.model.get('first_name'); 
        }

        return ''
      }
    }));
    this.$el.html(compiledHTML);

    return this;
  }
});
