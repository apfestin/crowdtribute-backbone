var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.InstructorPageView = Backbone.View.extend({
  tagName: 'div',
  className: 'instructor-page',
  initialize: function(options) {
    this.courseID = parseInt(options.course_id);
    this.instructorID = parseInt(options.instructor_id);

    this.courseModel = new CrowdTribute.Models.Course({ id: this.courseID });
    this.instructorModel = new CrowdTribute.Models.InstructorModel({ id: this.instructorID, course_id: this.courseID });


    this.courseMaterialCollection = new CrowdTribute.Collections.CourseMaterialCollection(null, {course_id: this.courseID, instructor_id: this.instructorID});
    this.courseMaterialListView = new CrowdTribute.Views.CourseMaterialListView({collection: this.courseMaterialCollection,
                                                                                course_id: this.courseID, 
                                                                                instructor_id: this.instructorID});
  },
  render: function() {
    
    var courseDescriptionView = new CrowdTribute.Views.CourseDescriptionView({model: this.courseModel});
    var instructorDescriptionView = new CrowdTribute.Views.InstructorDescriptionView({model: this.instructorModel});

    this.$el.append(courseDescriptionView.render().el);
    this.$el.append(instructorDescriptionView.render().el);
    this.$el.append(this.courseMaterialListView.render().el);

    var formTemplate = _.template($('#course-material-form-template').html());

    var self = this;

    var compiledFormTemplate = formTemplate({
      safeCourseID: function() {
        return self.courseID;
      },
      safeInstructorID: function() {
        return self.instructorID;
      }
    });

    this.$el.append(compiledFormTemplate);

    window.fff = this.$el;
    
    return this;
  },
  events : {
    'click #save-course-material-btn' : 'createCourseMaterial',
    'submit #course-material-form' : 'submitForm'
  },

  createCourseMaterial: function() {
    // this.collection.create({course_id: this.collection.courseID, 
    //                         instructor_id: this.collection.instructorID,
    //                         title: title, 
    //                         description: description,
    //                         file: file});

  },

  submitForm: function(e) {
    form = $(e.currentTarget)
    e.preventDefault();
    form.ajaxSubmit({success: this.successCallback(this), error: this.errorCallBack});
  },

  successCallback: function(self) {
    setTimeout(function() {
      console.log('hh');
      self.courseMaterialCollection.fetch({reset: true})
    }, 500);
  },

  errorCallBack: function() {
    console.log('EROOORRRRRR');
  }
});