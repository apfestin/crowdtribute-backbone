var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.InstructorListView = Backbone.View.extend({
  tagName: 'table',
  className: 'table table-striped',
  initialize: function() {
    this.listenTo(this.collection, 'reset', this.render);

    this.collection.fetch({reset: true});
  },
  render: function() {
    var self = this;

    _.each(this.collection.models, function(instructor) {
      instructorView = new CrowdTribute.Views.InstructorView({model: instructor, course_id: self.collection.courseID});
      self.$el.append(instructorView.render().el);
    });
    return this;
  }
});