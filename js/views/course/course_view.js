var CrowdTribute =  CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.CourseView = Backbone.View.extend({
  tagName: 'div',
  className: 'col-lg-4',
  template: CrowdTribute.Templates.CourseTemplate,
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    this.listenTo(this.model, 'destroy', this.remove);
  },
  render: function() {
    var self = this;
    compiledHTML = this.template(_.extend(this.model.toJSON(), { safeID: function() {
      if(self.model.get('id')) {
        return self.model.id;
      }
      return ''
    }}));


    this.$el.html(compiledHTML);

    return this;
  },

  events: {
    'click .edit-btn' : 'editCourse',
    'click .update-btn' : 'updateCourse',
    'click .delete-btn' : 'deleteCourse',
    'click .cancel-btn' : 'cancelEdit'
  },

  editCourse: function() {
    console.log('edit');
    this.$('.editable').show();

  },

  updateCourse: function() {
    this.model.save({name: this.$('.name-editable').val(), 
                     title: this.$('.title-editable').val()});

    this.$('.editable').hide();
  },

  deleteCourse: function() {
    console.log('delete');
    this.model.destroy();
  },

  cancelEdit: function() {
    console.log('fff');
    this.$('.editable').hide();
  }
});