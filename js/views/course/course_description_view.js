var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.CourseDescriptionView = Backbone.View.extend({
  tagName: 'div',
  template: _.template("<h2>Course: <a class='go-to-course-btn'><%= name %></a></h2>"),
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);

    this.model.fetch({reset: true});
  },
  render: function() {
    var compiledHTML = this.template(this.model.toJSON());
    this.$el.html(compiledHTML);

    return this;
  },
  events: {
    'click .go-to-course-btn' : 'goToCourse'
  },
  goToCourse: function() {
    CrowdTribute.App.navigate('courses/' + this.model.get('id'), {trigger: true});
  }
});