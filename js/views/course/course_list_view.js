var CrowdTribute =  CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.CourseListView = Backbone.View.extend({
  el: $('#body'),
  initialize: function() {
    this.listenTo(this.collection, 'reset', this.addAll);
    this.listenTo(this.collection, 'add', this.addOne);

    this.collection.fetch({reset: true});

    this.$el.append($('#add-new-course-template').html());
  },

  events: {
    'click #save-btn' : 'createCourse'
  },

  render: function() {
    return this;
  },

  addOne: function(course) {
    courseView = new CrowdTribute.Views.CourseView({model: course}); 
     this.$el.append(courseView.render().el)
  },

  addAll: function() {
    this.collection.each(this.addOne, this);
  },

  createCourse: function() {
    this.$('#success-msg').hide();

    var name = this.$('#name-field');
    var title = this.$('#title-field');
    var callBack = {success: function() {
                                name.val('');
                                title.val('');
                                this.$('#success-msg').show();
                             },
                    validate: true
                  };
    this.collection.create({name: name.val(), title: title.val()}, callBack);
  }
});