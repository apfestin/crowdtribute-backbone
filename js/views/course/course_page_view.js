var CrowdTribute = CrowdTribute || {};
CrowdTribute.Views = CrowdTribute.Views || {};

CrowdTribute.Views.CoursePageView = Backbone.View.extend({
  tagName: 'div',
  className: 'course-page',
  initialize: function(options) {
    this.courseID = parseInt(options.course_id);
  },
  render: function() {

    this.$el.html('');
    var instructorCollection = new CrowdTribute.Collections.InstructorCollection(null, {course_id: this.courseID});
    var instructorListView = new CrowdTribute.Views.InstructorListView({collection: instructorCollection});

    var courseModel = new CrowdTribute.Models.Course({ id: this.courseID });
    var descriptionView = new CrowdTribute.Views.CourseDescriptionView({model: courseModel});

    this.$el.append(descriptionView.render().el);
    this.$el.append(instructorListView.render().el);
    this.$el.append('<div><button class="btn btn-default" id="back-to-courses-btn"> Back to List of Courses</button></div>');
    
    return this;
  },
  events: {
    'click #back-to-courses-btn' : 'goBackToCourses'
  } ,
  goBackToCourses: function() {
    CrowdTribute.App.navigate('', {trigger: true});
  }
});