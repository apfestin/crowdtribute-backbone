var CrowdTribute = CrowdTribute || {};
CrowdTribute.Collections = CrowdTribute.Collections || {};

CrowdTribute.Collections.InstructorCollection = Backbone.Collection.extend({
  model: CrowdTribute.Models.InstructorModel,
  initialize: function(models, options) {
    this.courseID = parseInt(options.course_id)
  },
  url: function() {
        return 'http://localhost:3000/courses/' + this.courseID + '/instructors';
      }
});