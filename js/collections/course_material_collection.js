var CrowdTribute = CrowdTribute || {};
CrowdTribute.Collections = CrowdTribute.Collections || {};

CrowdTribute.Collections.CourseMaterialCollection = Backbone.Collection.extend({
  model: CrowdTribute.Models.CourseMaterialModel,
  initialize: function(models, options) {
    this.courseID = parseInt(options.course_id);
    this.instructorID = parseInt(options.instructor_id);
  },
  url: function() {
        return 'http://localhost:3000/courses/' + this.courseID + '/instructors/' + this.instructorID + '/course_materials';
    }
});