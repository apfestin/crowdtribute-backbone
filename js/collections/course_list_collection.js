var CrowdTribute = CrowdTribute || {};
CrowdTribute.Collections = CrowdTribute.Collections || {};

CrowdTribute.Collections.CourseList = Backbone.Collection.extend({
  model: CrowdTribute.Models.Course,
  url: 'http://localhost:3000/courses'
});
